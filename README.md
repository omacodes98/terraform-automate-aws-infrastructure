# Demo Project - Automate AWS infastructure with Terraform

## Table of Contents

- [Project Description](#project-description)

- [Technologies Used](#technologies-used)

- [Steps](#steps)

- [Installation](#installation)

- [Usage](#usage)

- [How to Contribute](#how-to-contribute)

- [Questions](#questions)

- [References](#references)

- [License](#license)

## Project Description

* Create TF project to automate provisioning AWS infastructure and its components, such as: VPC,Subnet,Route Table,Internet Gateway,EC2, Security Group 

* Configure TF script to automate deploying Docker container to EC2 instance 

## Technologies Used 

* Terraform

* AWS

* Docker

* Linux 

* Git 

## Steps 

Step 1: Create Terraform config file which will have vpc and subnet configuration in it 

[Terraform Config File creation](/images/01_create_terraform_config_file_to_create_vpc_and_subnets.png)

Step 2: Create variables file to have variables that are needed to allow file to be dynamic 

[Variables file](/images/02_create_variable_files_to_have_dynamic_variables_that_can_be_easily_changed_depending_on_staging_env_or_cidr_blocks.png)


Step 3: Apply changes with terraform apply in terminal 

    terraform apply -var-file terraform-dev.tfvars

[Terraform apply](/images/03_terraform_apply_creation.png)
[Vpc and Subnet Created](/images/04_vpc_subnet_created.png)
[Vpc on AWS](/images/05_vpc_on_aws.png)
[Subnet on AWS](/images/06_subnet_on_aws.png)

Step 4: Create route table for the vpc to communicate to the internet gateway which will enable access to the internet from any external ip address 

note: You do not need to configure a route for communication in vpc because its automatically done by aws

[Route Table configuration](/images/07_create_route_for_the_vpc_to_communicate_to_the_internet_gateway_which_will_enable_access_to_the_internet_from_any_external_ip_address_note_you_do_not_need_to_configure_a_route_for_communication_in_vpc_because_its_automatically_done_by_aws.png)

Step 5: Create a gateway for the route to communicate to the internet 

[Gateway Creation](/images/08_create_a_gateway_for_the_route_to_communicate_to_the_internet.png)

Step 6: Apply changes 

    terraform apply -var-file terraform-dev.tfvars

[Apply Changes 1](/immages/09_apply_tf_config_file.png)

[Apply Changes 2](/images/09_apply_tf_config_file1.png)

[Route table on AWS](/images/10_route_table_on_aws.png)


Step 7: Associate subnet to the route table so that route can control the comunication of networks in subnet and gateway. To do this you will need to create a route table associate in terraform main fill with apropriate configurations

[Route table associate](/images/11_associate_subnet_to_the_route_table_so_that_route_can_control_the_communications_of_networks_in_subnet_and_gateway_.png)

Step 8: Apply Changes 

    terraform apply -var-file terraform-dev.tfvars

[Apply Changes 3](/images/12_apply_tf_config_file.png)

[Subnet associate on AWS](/images/13_subnet_association_on_aws.png)

Step 8 (Default): Comment out route table and route table associate to use the main default route table that was created automatically from aws when creating the vpc 

[Comment out rt and rtb](/images/14_comment_out_route_table_and_route_table_associate_to_use_the_main_default_route_table_that_was_create_automatically_from_aws_when_creating_the_vpc.png) 

Step 9 (Default): Check for default route table id

    terraform state show aws_vpc.myapp-vpc

[Check for route table id](/images/15_check_for_default_route_table.png)

Step 10 (Default): Add resource for default route table in terraform main file and add default route table id from above step

[Default route table configuration](/images/16_add_resource_for_default_route_tabble_and_add_default_route_table_id_from_above_step.png)

Step 11 (Default): Apply Changes

    terraform apply -var-file terraform-dev.tfvars

[Apply Default Changes](/images/17_apply_tf_config_file.png)

[Default Route table on AWS](/Images/18_default_route_on_aws_new_route_for_gateway.png)

Step 12 (Default): In the terraform main file change route table id in aws route table association to the default route table id 

[Default route table association](/images/19_add_resource_to_associate_subnet_to_default_route_table.png)

Step 13 (Default): Apply changes 

    terraform apply -var-file terraform-dev.tfvars

[Apply Default Chnages 2](/images/20_apply_tf_config_file.png)

[Subnet Association](/images/21_subnet_associated_to_default_subnet_on_aws.png)


Step 14: Add resource for creation of security group, this is because we will need some ports open for our application and to enable us ssh into ec2 instance.

[Security Group](/images/22_create_resource_for_security_group_so_we_could_be_able_to_ssh_into_ec2_instance_and_also_be_able_to_access_application_through_the_browser.png)

Step 15: Add ingress rule as part of configuration for security group, this is for incoming traffic such as ssh. You will need to open port 22 to allow ssh and in cidr block add your ip address or ip address of members you want to have access into the server (ec2 instance)

[SSH ingress rule](/images/23_add_ingress_rule_for_incoming_traffic_such_as_ssh_open_port_22_and_in_cidr_block_add_your_ip_address_only_for_security_or_a_list_of_members_in_a_team_ip_address.png)

Step 16: Add ingress rule for application, in this case nginx which will mean we want port 8080 opened and we want to allow any ip address have access to enable customers to open application from browser 

[Nginx ingress rule](/images/24_add_ingress_rule_for_nginx_by_opening_port_8080_and_allow_cidr_block_to_any_ip_address.png)

Step 17: Add egress for exiting traffic, this is in situation like installing things on server specifically fetching docker images from repo 

[Egress rule](/images/25_add_egress_for_exiting_traffic_rule_for_things_like_installing_things_on_server_specifically_fetching_docker_images.png)

Step 18: Apply changes 

[Apply changes 4](/images/26_apply_tf_config_file_.png)

[Apply changes 5](/images/26_apply_tf_config_file_2.png)

Step 19: Create resource for ec2 instance and add ami id of your choice 

[ec2 ami id](/images/30_create_resource_for_ec2_instance_which_has_ami_id.png)

Step 20: Add data because images already exist in data and add owners and necessary filter to enable ease of finding image

Step 21: Add instance type in aws instance resource 

[instance type](/images/34_add_instance_type_in_aws_instance_resource.png)

Step 22: Add subnet id in aws instance resource 

[subnet id added](/images/35_add_subnet_id.png)

Step 23: Add security group id in aws instance resource 

[security group added](/images/36_add_security_group_id.png)

Step 24: Add availability zone in aws instance resource 

[avalability zone added](/images/37_add_availability_zone.png)

Step 25: Associate public ip address to true in order to access application through browser 

[Public ip address](/images/38_set_associate_public_ip_address_to_true_this_is_because_we_want_to_access_application_through_the_browser.png)

Step 26: Use ssh keygen to create public and private key 

[keygen](/images/49_use_ssh_keygen_to_create_public_and_private_key.png)

Step 27: Create key pair resource in main config file 

[Key pair creation](/images/48_you_could_also_automate_creation_of_public_key_for_ec2_instance_by_creating_resource_aws_key_pair_give_this_resource_key_name_and_public_key.png)

Step 28: Add key name to aws instance resource 

[Adding key pair to instance](/images/50_change_key_name_in_aws_instance_to_the_newly_configured_key_name.png)

Step 29: Apply changes 

[Apply changes 6](/images/53_apply_changes_to_configuration.png)

[Apply changes 7](/images/53_apply_changes_to_configuration_1.png)

Step 30: Check for public ip address of ec2 instance 

    terraform state show aws_instance.myapp_server

[public ip address check](/images/54_check_for_the_public_ip_of_ec2_instance_on_terraform.png)

Step 31: ssh into the ec2 instance using the private key that was created 

    ssh -i ~/.ssh/id_ed25519 ec2-user@35.178.144.76

[ssh into server](/images/55_ssh_into_ec2_instance_using_the_private_key_of_the_of_the_key-air_you_created_with_keygen.png)

Step 32: Add a component in aws instance resource called user_data and execute a script that downloads nginx as a docker container 

[User data](/images/57_add_a_component_in_aws_instance_resource_called_user_data_and_execute_a_script_that_downloads_nginx_as_a_docker_container.png)

Step 33: Apply changes 

[Apply changes 8](/images/59_apply_changes_to_configuration.png)

[Apply changes 9](/images/59_apply_changes_to_configuration_1.png)

Step 34: Ssh into server and check if nginx is running as a docker container

    ssh -i ~/.ssh/id_ed25519 ec2-user@35.178.144.76

    docker ps 


[Check nginx container](/images/60_ssh_into_server_and_check_if_nginx_is_running_as_a_docker_container.png)

Step 35: Check nginx on browseer 

[nginx on browser](/images/61_nginx_on_browser.png)

Step 36: You could also extract script 

[extract script](/images/62_extract_the_shell_script_and_past_it_inside_its_own_file.png)

[script created](/images/63_script_created.png)

Step 37: Apply changes 

[Apply changes](/images/65_apply_changes_1.png)

[ssh into server to check](/images/66_ssh_into_server_and_check_if_the_script_was_successful.png)

[nginx on browser change](/images/67_nginx_on_browser.png)

## Installation 

    brew install terraform 

## Usage 

    Terraform apply 

## How to Contribute

1. Clone the repo using $ git clone git@gitlab.com:omacodes98/terraform-automate-aws-infrastructure.git

2. Create a new branch $ git checkout -b your name 

3. Make Changes and test 

4. Submit a pull request with description for review



## Questions

Feel free to contact me for further questions via: 

Gitlab: https://gitlab.com/omacodes98

Email: omacodes98@gmail.com

## References

https://gitlab.com/omacodes98/terraform-automate-aws-infrastructure

## License

The MIT License 

For more informaation you can click the link below:

https://opensource.org/licenses/MIT

Copyright (c) 2022 Omatsola Beji.